package com.example.yuri.rotateanimationexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("collapse");
        if (fragment==null) {
            Log.d(TAG, "New CollapseFragment created.");
            fragment = new CollapseFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment, "collapse")
                    .commit();
        }
    }


}
