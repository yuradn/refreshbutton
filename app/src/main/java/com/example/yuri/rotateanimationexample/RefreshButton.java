package com.example.yuri.rotateanimationexample;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by test on 4/5/16.
 */
@CoordinatorLayout.DefaultBehavior(RefreshButton.ShrinkBehavior.class)
public class RefreshButton extends FrameLayout {
    private final static String TAG = "RefreshButton";
    private RotateAnimation rotate = null;
    ImageView imageButton;

    public RefreshButton(Context context) {
        super(context);
        init();
    }

    public RefreshButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        imageButton = new ImageView(getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        imageButton.setLayoutParams(params);
        imageButton.setImageResource(R.mipmap.icon_refresh_button);
        imageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                performClick();
            }
        });
        addView(imageButton);
    }


    @Override
    public boolean performClick() {
        //Log.d(TAG, "performClick(), view: " + getRootView().getClass().getName());
        //getChildView((ViewGroup) RefreshButton.this.getRootView());
        if (isAnimation()) {
            isAnimation();
            super.performClick();
        } else {
            rotate.cancel();
            rotate = null;
        }
        return true;
    }


    private boolean isAnimation() {
        if (rotate != null) return false;
        getAnim();
        return true;
    }

    private void getAnim() {
        rotate = new RotateAnimation(0, 358, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        //rotate.setAnimationListener(this);
        rotate.setDuration(800);
        rotate.setInterpolator(new MyInterpolator());
        rotate.setRepeatCount(Animation.INFINITE);


        imageButton.startAnimation(rotate);
    }

    class MyInterpolator implements Interpolator {
        @Override
        public float getInterpolation(float t) {
            return t;
        }
    }

    public static class ShrinkBehavior extends CoordinatorLayout.Behavior<RefreshButton> {

        public ShrinkBehavior() {
        }

        public ShrinkBehavior(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean layoutDependsOn(CoordinatorLayout parent, RefreshButton child, View dependency) {
            return dependency instanceof Snackbar.SnackbarLayout;
        }

        @Override
        public boolean onDependentViewChanged(CoordinatorLayout parent, RefreshButton child, View dependency) {
            float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
            child.setTranslationY(translationY);
            return true;
        }
    }
}
